# number_trivia

[![pipeline status](https://gitlab.com/LisaAnthonioz/number_trivia/badges/master/pipeline.svg)](https://gitlab.com/LisaAnthonioz/number_trivia/-/commits/master)
[![coverage report](https://gitlab.com/LisaAnthonioz/number_trivia/badges/master/coverage.svg)](https://gitlab.com/LisaAnthonioz/number_trivia/-/commits/master)

For this project I followed a [tutorial](https://resocoder.com/flutter-clean-architecture-tdd/) from [Matt Rešetár](https://twitter.com/resocoder).
